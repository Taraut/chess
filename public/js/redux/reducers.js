const initialState = [
    {
        titles: ['No titles (reducer default)'],
        itemUrl: '/'
    }
]

const itemsReducer = (state = initialState, action) => {

    // GET_ITEMS_TITLE
    switch (action.type) {
        case 'GET_ITEMS_TITLE':
            return Object.assign({}, state, {
                titles: action.titles,
                itemUrl: action.itemUrl
              });
        default:
            return state;
    }

};

export {itemsReducer};
