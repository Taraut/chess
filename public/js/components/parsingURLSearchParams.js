// call example: const urlSearchData = parsingURLSearchParams(this.props.location.search);

const parsingURLSearchParams = (thisPropsLocationSearch) => {
    const searchParams = new URLSearchParams( thisPropsLocationSearch );
    let searchData = {};
    searchParams.forEach( (value, key) => {
        searchData[key] = value;
      });
    return searchData;
}

module.exports = parsingURLSearchParams;
