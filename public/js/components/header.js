import { Icon } from 'antd';

class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <div className="header_leftBlock">
                    <div className="header_c">
                        <div className="header_c_inside">C</div>
                    </div>
                    <div className="header_coachess">
                        <div className="header_coachess_text">
                            <span className="header_coachess_coa">Coa</span>
                            <span className="header_coachess_chess">Chess</span>
                        </div>
                    </div>
                </div>

                <div className="header_rightBlock">
                    <Icon type="user"  />
                </div>
            </div>
        );
    }
}

export default Header;
