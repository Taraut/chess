const parsingURLSearchParams = require('./parsingURLSearchParams');

const searchParams = '?testNumber=12&testtext=TestText';
const searchData = {
    testNumber: '12',
    testtext: 'TestText'
};

test('Search params parsed to object', () => {
    expect(parsingURLSearchParams(searchParams)).toMatchObject(searchData);
});


const searchParams1 = '';
const searchData1 = {};

test('Search w/o params parsed to empty object', () => {
    expect(parsingURLSearchParams(searchParams1)).toMatchObject(searchData1);
});
