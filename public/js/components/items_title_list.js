import React from 'react';
import { connect } from 'react-redux';

const ItemsTitleList = props => {
    const titles = props.titles;
    return (
        <h3>
            Page title list (Redux driven):
            {titles.map((value, index) => {
                return <li key={index}>{value}</li>;
            })}
        </h3>
    );
};

const mapStateToProps = state => {
    return {
        titles: state.itemsReducer.titles
    };
};

export default connect(mapStateToProps)(ItemsTitleList);
