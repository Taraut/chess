import React from 'react';
import axios from 'axios';
import parsingURLSearchParams from './parsingURLSearchParams';
import { Row, Col, Alert, Spin } from 'antd';
import { MyButton, TextTitle, TextSimple } from './atoms';
import ItemsTitleList from './items_title_list';

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            id: 1,
            isLoaded: true,
            error: false,
            backButton: {
                iconType: 'left'
            }
        };

        this.urlSearchParser = this.urlSearchParser.bind(this);
        this.backToItemsTable = this.backToItemsTable.bind(this);
    }

    //../item?currentPage=${currentPage}&key=${key}
    componentDidMount() {
        const urlSearchData = parsingURLSearchParams(this.props.location.search);
        this.urlSearchParser(urlSearchData);
    }

    backToItemsTable() {
        const { currentPage } = this.state;
        const url = `../?currentPage=${currentPage}`;

        window.location.assign(url);
    }

    urlSearchParser(urlSearchData) {
        if (urlSearchData.currentPage) {
            this.setState({
                currentPage: urlSearchData.currentPage
            });
        }

        if (!urlSearchData.id) {
            this.setState({
                error: 'No Item Id provided'
            });
            return;
        }

        this.getOneItemById(urlSearchData.id);
    }

    // http://localhost:3000/itemsonebyid?id=1
    getOneItemById(id) {
        const apiUrl = `../itemsonebyid?id=${id}`;
        this.setState({
            isLoaded: false
        });

        axios
            .get(apiUrl)
            .then(res => {
                const { item } = res.data;
                const error = res.data.error ? res.data.error : false;

                this.setState({
                    item: item[0],
                    error: error,
                    isLoaded: true
                });
            })
            .catch(error => {
                this.setState({
                    item: [],
                    error: error.toString(),
                    isLoaded: true
                });
            });
    }

    render() {
        const { error, isLoaded, item, backButton } = this.state;

        const title = item ? item.title : false;
        const text = item ? item.text : false;

        return (
            <Row>
                <Col>
                    {error && (
                        <Alert
                            message="Error"
                            description={error}
                            type="error"
                            showIcon
                            closable
                            onClose={this.backToItemsTable}
                        />
                    )}

                    {!isLoaded && <Spin size="large"></Spin>}

                    {title && <TextTitle text={title} />}

                    {text && <TextSimple text={text} />}

                    <MyButton data={backButton} text="Back" onClick={this.backToItemsTable} />
                </Col>
                <Col>
                    <ItemsTitleList />
                </Col>
            </Row>
        );
    }
}

export default Item;
