import React from 'react';
import { connect } from 'react-redux';
import ItemsTable from './items_table';
import { getItemsTitle } from '../redux/actions';
import parsingURLSearchParams from './parsingURLSearchParams';

const ItemsPage = props => {
    const urlSearchData = parsingURLSearchParams(props.location.search);
    return (
        <div>
            <ItemsTable
                menuOnClick={ (data) => {
                    props.dispatch(getItemsTitle(data));

                    props.history.push(data.itemUrl);
                }}
                urlSearchData={urlSearchData}
            />
        </div>
    );
};

export default connect()(ItemsPage);
