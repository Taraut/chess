import React from 'react';
import axios from 'axios';
import { Row, Col, Menu, Alert, Pagination } from 'antd';
import PropTypes from 'prop-types';

class ItemsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            itemsPagesCount: 0,
            error: false
        };

        this.paginatorOnChange = this.paginatorOnChange.bind(this);
        this.menuOnClick = this.menuOnClick.bind(this);
    }

    // http://localhost:3000/?currentPage=3
    componentDidMount() {
        const urlSearchData = this.props.urlSearchData;
        const page = Number.parseInt(urlSearchData.currentPage)
            ? Number.parseInt(urlSearchData.currentPage)
            : 1;
        this.paginatorOnChange(page);
    }

    getItems() {
        const { currentPage } = this.state;
        const apiUrl = `../itemsall?page=${currentPage}`;

        axios
            .get(apiUrl)
            .then(res => {
                const { itemsPagesCount, items } = res.data;
                const error = res.data.error ? res.data.error : false;

                let titles = [];
                items.forEach(item => {
                    titles.push(item.title);
                });

                this.setState({
                    itemsPagesCount: itemsPagesCount,
                    items: items,
                    titles: titles,
                    error: error
                });
            })
            .catch(error => {
                this.setState({
                    itemsPagesCount: 0,
                    items: [],
                    error: error.toString()
                });
            });
    }

    paginatorOnChange(page) {
        this.setState(
            {
                currentPage: page
            },
            () => this.getItems()
        );
    }

    menuOnClick({ key }) {
        const { currentPage } = this.state;
        const url = `../item?currentPage=${currentPage}&id=${key}`;

        this.props.menuOnClick({
            titles: this.state.titles,
            itemUrl: url
        });
    }

    render() {
        const { items, error, itemsPagesCount, currentPage } = this.state;

        return (
            <Row>
                <Col>
                    {error && (
                        <Alert message="Error" description={error} type="error" showIcon closable />
                    )}

                    {items && (
                        <Row>
                            <Menu
                                onClick={this.handleClick}
                                style={{ width: '100%' }}
                                defaultSelectedKeys={['0']}
                                defaultOpenKeys={['sub1']}
                                mode="inline"
                                onClick={this.menuOnClick}
                            >
                                <Menu.ItemGroup key="g1" title="Items Table">
                                    {items.map(item => (
                                        <Menu.Item key={item.id} className="itemsTable_menuItem">
                                            <Row>
                                                <Col span={4} className="itemsTable_title">
                                                    {item.title}
                                                </Col>
                                                <Col span={20} className="itemsTable_text">
                                                    {item.text}
                                                </Col>
                                            </Row>
                                        </Menu.Item>
                                    ))}
                                </Menu.ItemGroup>
                            </Menu>

                            <Pagination
                                current={currentPage}
                                defaultPageSize={1}
                                total={itemsPagesCount}
                                onChange={this.paginatorOnChange}
                                hideOnSinglePage={true}
                            />
                        </Row>
                    )}
                </Col>
            </Row>
        );
    }
}

export default ItemsTable;

ItemsTable.propTypes = {
    urlSearchData: PropTypes.object
};
ItemsTable.defaultProps = {
    urlSearchData: {}
};
