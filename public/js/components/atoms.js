import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from 'antd';

const MyButton = ({ data, text, onClick }) => {
    const { type = 'primary', size = 'large', iconType = false } = data;
    return (
        <Button type={type} size={size} onClick={onClick}>
            {onClick && <Icon type={iconType} />}

            <span className='MyButton_Text'>{text}</span>
        </Button>
    );
};

const TextTitle = ({ text }) => {
    return <div className="textTitle">{text}</div>;
};

const TextSimple = ({ text }) => {
    return <div className="textSimple">{text}</div>;
};


MyButton.propTypes = {
    data: PropTypes.object,
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};
TextTitle.defaultProps = {
    text: 'Button'
};

TextTitle.propTypes = {
    text: PropTypes.string
};
TextTitle.defaultProps = {
    text: 'No text'
};

TextSimple.propTypes = {
    text: PropTypes.string
};
TextSimple.defaultProps = {
    text: 'No text'
};

export { TextTitle, TextSimple, MyButton };
