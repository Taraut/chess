import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { Row, Col } from 'antd';
import Header from './components/header';
import ItemsPage from './components/items_page';
import Item from './components/item';

const AppRouter = () => (
    <BrowserRouter>
        <Row>
            <Header />
            <Col className="horizontalMenu">Horizontal menu</Col>
            <Col span={2} />
            <Col span={20} className="mainPart">
                <nav>
                    <Switch>
                        <Route exact path="/" component={ItemsPage} />
                        <Route path="/item" component={Item} />
                    </Switch>
                </nav>
            </Col>
            <Col span={2} />
        </Row>
    </BrowserRouter>
);

export default AppRouter;
