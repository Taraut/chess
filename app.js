const path = require('path');
const express = require('express');
const endpItemsAll = require ('./endp/endp_items_all');
const endpItemsOneById = require ('./endp/endp_items_one_by_id');

const app = express();

// eslint-disable-next-line no-undef
const publicDirectoryPath = path.join(__dirname, './public');
app.use(express.static(publicDirectoryPath));

// view engine setup
// eslint-disable-next-line no-undef
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile); // it works
app.set('view engine', 'html');

app.get('/itemsonebyid', (req, res) => {
    endpItemsOneById(req, res);
});

app.get('/itemsall', (req, res) => {
    endpItemsAll(req, res);
});

app.get('*', function(req, res) {
    res.render('index');
});

module.exports = app
