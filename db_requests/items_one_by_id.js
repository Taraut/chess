const { conDb1 } = require('./items_requests');

const itemsOneById = (key=0, callback) => {

    let requestedQuery = `SELECT * FROM items WHERE id=${key}`;

    conDb1(requestedQuery, result => {
        if (result.fatal) {
            callback({
                error: result.code
            });
            return console.log('ERROR:', result.code);
        }
        const data = JSON.parse(JSON.stringify(result));
        callback(data);
    });

};

module.exports = itemsOneById;
