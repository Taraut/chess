const mysql = require('mysql');
const { conDataDb1} = require('../defaults');

const conDb1 = (requestedQuery, callback) => {

    const connectDb1 = mysql.createConnection(conDataDb1);

    connectDb1.connect(error => {
        if (error) {
            callback(error);
            connectDb1.end();
            return;
        }

        connectDb1.query(requestedQuery, (error, result) => {
            if (error) {
                callback(error);
                return;
            }
            callback(result);
            connectDb1.end();
        });
    });
};

module.exports.conDb1 = conDb1;
