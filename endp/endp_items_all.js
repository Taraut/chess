const itemsAll = require('../db_requests/items_all.js');
const pagesCount = require('../db_requests/pages_count.js');

const endpItemsAll = (req, res) => {
    let page = 0; // if page 0 or Error all items will be sent.
    if (Number(req.query.page)) {
        page = Number(req.query.page);
    }

    pagesCount(' ', pagesQtt => {
        if (pagesQtt.error) {
            res.send({
                itemsPagesCount: 0,
                items: [],
                error: pagesQtt.error
            });
            return;
        }

        if (pagesQtt > 0) {
            itemsAll(page, response => {
                if (response.error) {
                    res.send({
                        itemsPagesCount: 0,
                        items: [],
                        error: response.error
                    });
                    return;
                }
                res.send({
                    itemsPagesCount: pagesQtt,
                    items: response
                });
            });
            return;
        }

        if (pagesQtt <= 0) {
            res.send({
                itemsPagesCount: 0,
                items: []
            });
        }
    });
};

module.exports = endpItemsAll;
