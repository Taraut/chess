Backend part

The main goal is to create React app which gathers simplest data from database and displays it
on the screen. For single page React application you can use create-react-app or any preferable
react template.

For backend stuff please use Mysql and Express and run them on simplest Node instance with
webpack f.e. Database should consist of only one table called "items".

"Items" table should contain only three columns: id, title, text. Each column should have proper,
production-ready data type. Fill table with few items for further actions. Please use generated text
for item contents from here: https://www.lipsum.com/feed/html;

Express method should make simple mysql request (use any ORM, or Query builder you prefer
the most) which should return response with JSON object like {id, title, text}.

Frontend part

Main goal is to read data from backend and to display it on the screen in two ways:

1. Item list view.

Print an unordered list of items vertically directed, styling them on your own. So 20 database
items should be divided to pages. Each page has 5 items and pagination below the list.
For switching between two pages please use state/hooks.
Each item should consist of a title and a limited text string (300 chars). Please try to use Styled
Components for decoration or otherwise use SCSS in most proper way you can.

2. Opened item.

When clicking an an item in list, view switches to an item page. Here you see clickable back
navigation button, item title and item text. All style decorations, colours, shadows etc. will benefit.
For both views please add the simplest header you can create. Here is an example of ours just for
inspiration purposes:

Tips

If possible try to build few components using functional components and few using class
components.
