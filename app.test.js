const request = require('supertest');
const app = require('./app');

describe('GET /', () => {
    test(' / should response with code 200', async () => {
        const response = await request(app).get('/');
        expect(response.statusCode).toBe(200);
    });
});

// http://localhost:3000/itemsall?page=1
describe('GET /itemsall', () => {
    test(' /itemsall should response with code 200 and {itemsPagesCount..., items}', async () => {
        const response = await request(app).get('/itemsall');
        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('itemsPagesCount');
        expect(response.body).toHaveProperty('items');
    });
});

